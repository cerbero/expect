<?php

/**
 * Test Expect PHPUnit wrapper.
 *
 * @author	Andrea Marco Sartori
 */
class TestExpect extends \PHPUnit_Framework_TestCase
{

	/**
	 * @testdox	Assertions can have multiple expectations.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAssertionsCanHaveMultipleExpectations()
	{
		$array1 = array('foo', 'bar');

		$array2 = array('baz');

		Expect::that($array1, $array2)->have('bar', 'baz');

		try
		{
			Expect::that($array1, $array2)->have('foo', 'bar');
		}
		catch (PHPUnit_Framework_AssertionFailedError $e)
		{
			return;
		}
		$this->fail();
	}

	/**
	 * @testdox	New aliases can be registered.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testNewAliasesCanBeRegistered()
	{
		Expect::register(array('newAlias' => 'assertTrue'));

		Expect::that(true)->newAlias();

		try
		{
			Expect::that(false)->newAlias();
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            return;
        }
        $this->fail();
	}

	/**
	 * @testdox	isTrue succeed when testing true.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testIsTrueSucceedWhenTestingTrue()
	{
		Expect::that(true)->isTrue();
	}

	/**
	 * @testdox	isTrue fails when testing false.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testIsTrueFailsWhenTestingFalse()
	{
		try
		{
            Expect::that(false)->isTrue();
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            return;
        }
        $this->fail();
	}

	/**
	 * @testdox	areTrue succeed when testing more trues.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAreTrueSucceedWhenTestingMoreTrues()
	{
		Expect::that(true, true)->areTrue();
	}

	/**
	 * @testdox	areTrue fails when testing false.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAreTrueFailsWhenTestingFalse()
	{
		try
		{
            Expect::that(true, false)->areTrue();
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            return;
        }
        $this->fail();
	}

	/**
	 * @testdox	isFalse succeed when testing false.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testIsFalseSucceedWhenTestingFalse()
	{
		Expect::that(false)->isFalse();
	}

	/**
	 * @testdox	isFalse fails when testing true.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testIsFalseFailsWhenTestingTrue()
	{
		try
		{
            Expect::that(true)->isFalse();
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            return;
        }
        $this->fail();
	}

	/**
	 * @testdox	areFalse succeed when testing more falses.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAreFalseSucceedWhenTestingMoreFalses()
	{
		Expect::that(false, false)->areFalse();
	}

	/**
	 * @testdox	areFalse fails when testing true.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAreFalseFailsWhenTestingTrue()
	{
		try
		{
            Expect::that(false, true)->areFalse();
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            return;
        }
        $this->fail();
	}

	/**
	 * @testdox	has succeed when something is in array.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHasSucceedWhenSomethingIsInArray()
	{
		$array = array('foo', 'bar');

		Expect::that($array)->has('bar');
	}

	/**
	 * @testdox	has fails when something is not in array.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHasFailsWhenSomethingIsNotInArray()
	{
		$array = array('foo', 'bar');

		try
		{
			Expect::that($array)->has('baz');
		}
		catch (PHPUnit_Framework_AssertionFailedError $e)
		{
			return;
		}
		$this->fail();
	}

	/**
	 * @testdox	has succeed when all values are in array.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHasSucceedWhenAllValuesAreInArray()
	{
		$array = array('foo', 'bar');

		Expect::that($array)->has('foo', 'bar');
	}

	/**
	 * @testdox	has fails when not all values are in array.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHasFailsWhenNotAllValuesAreInArray()
	{
		$array = array('foo', 'bar');

		try
		{
			Expect::that($array)->has('foo', 'baz');
		}
		catch (PHPUnit_Framework_AssertionFailedError $e)
		{
			return;
		}
		$this->fail();
	}

	/**
	 * @testdox	have succeed when something is in all arrays.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHaveSucceedWhenSomethingIsInAllArrays()
	{
		$array1 = array('foo', 'bar');

		$array2 = array('bar');

		Expect::that($array1, $array2)->have('bar');
	}

	/**
	 * @testdox	have fails when something is not in all arrays.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHaveFailsWhenSomethingIsNotInAllArrays()
	{
		$array1 = array('foo', 'bar');

		$array2 = array('bar');

		try
		{
			Expect::that($array1, $array2)->have('baz');
		}
		catch (PHPUnit_Framework_AssertionFailedError $e)
		{
			return;
		}
		$this->fail();
	}

	/**
	 * @testdox	have succeed when multiple values are in all arrays.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHaveSucceedWhenMultipleValuesAreInAllArrays()
	{
		$array1 = array('foo', 'bar');

		$array2 = array('foo', 'baz');

		Expect::that($array1, $array2)->have(array('foo', 'bar'), array('foo', 'baz'));
	}

	/**
	 * @testdox	have fails when multiple values are not in all arrays.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testHaveFailsWhenMultipleValuesAreNotInAllArrays()
	{
		$array1 = array('foo', 'bar');

		$array2 = array('foo', 'baz');

		try
		{
			Expect::that($array1, $array2)->have(array('foo', 'bar'), array('foo', 'bar'));
		}
		catch (PHPUnit_Framework_AssertionFailedError $e)
		{
			return;
		}
		$this->fail();
	}

}