<?php

/**
 * PHPUnit wrapper.
 *
 * @author  Andrea Marco Sartori
 */
class Expect extends Expect\Wrapper
{
    
    /**
     * @author  Andrea Marco Sartori
     * @var     array   $aliases   Aliases for PHPUnit methods.
     */
    protected $aliases = array(

        '(is|are)True' => 'assertTrue',

        '(is|are)False' => 'assertFalse',

        '(has|have)' => 'assertContains',

    );

    /**
     * Retrieve the correct PHPUnit assertion name
     *
     * @author  Andrea Marco Sartori
     * @param   string  $methodName
     * @return  string
     */
    protected function getMethod($methodName)
    {
        foreach ($this->aliases as $alias => $method)
        {
            if(preg_match("/{$alias}/", $methodName)) return $method;
        }
        throw new \BadMethodCallException("Unable to call [$methodName] method.");
    }

    /**
     * Allow users to register their own aliases
     *
     * @author  Andrea Marco Sartori
     * @param   array   $aliases
     * @return  void
     */
    protected function addAliases(array $aliases)
    {
        foreach ($aliases as $alias)
        {
            $this->aliases = array_merge($alias, $this->aliases);
        }
    }

    /*----------------------------------------------------------------*\
     *    Existing methods overridden to make namesake aliases work   *
    \*----------------------------------------------------------------*/

    public static function isTrue()
    {
        static::getInstance()->areTrue();
    }

    public static function isFalse()
    {
        static::getInstance()->areFalse();
    }

}