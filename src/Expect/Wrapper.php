<?php namespace Expect;

/**
 * Must-extend class for PHPUnit wrappers.
 *
 * @author	Andrea Marco Sartori
 */
abstract class Wrapper extends \PHPUnit_Framework_TestCase
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$instance	Store class instance.
	 */
	protected static $instance = array();

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$actuals	Values to test against.
	 */
	protected $actuals = array();

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$staticCalls	Allowed static calls and relative methods.
	 */
	protected static $staticCalls = array(

		'that' => 'setActuals',

		'register' => 'addAliases',

	);

	/**
	 * Retrieve class singeton.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	Expect\Wrapper
	 */
	public static function getInstance()
	{
		$class = get_called_class();

		if( ! isset(static::$instance[$class]))
		{
			static::$instance[$class] = new static;
		}
		return static::$instance[$class];
	}

    /**
     * Set the actual values to test against.
     *
     * @author	Andrea Marco Sartori
     * @param	array	$parameter
     * @return	Expect\Wrapper	$this
     */
    public function setActuals()
    {
    	$this->actuals = func_get_arg(0);

    	return $this;
    }

    /**
     * Retrieve the correct PHPUnit assertion name
     *
	 * @author	Andrea Marco Sartori
     * @param	string	$methodName
     * @return	string
     */
    abstract protected function getMethod($methodName);

    /**
     * Allow users to register their own aliases
     *
     * @author	Andrea Marco Sartori
     * @param	array	$aliases
     * @return	void
     */
    abstract protected function addAliases(array $aliases);

    /**
     * Handle dynamic static calls.
     *
	 * @author	Andrea Marco Sartori
     * @param	string  $methodName
     * @param	array   $args
     * @return	Expect\Wrapper
     */
    public static function __callStatic($methodName, $args)
    {
    	foreach (static::$staticCalls as $call => $method)
    	{
    		if($methodName === $call)
    		{
    			return static::getInstance()->$method($args);
    		}
    	}
    	throw new \BadMethodCallException("Unable to call [$methodName] method.");
    }

    /**
     * Handle dynamic calls to PHPUnit methods.
     *
     * @author	Andrea Marco Sartori
     * @param	string	$methodName
     * @param	array	$args
     * @return	void
     */
    public function __call($methodName, $args)
    {
    	$method = $this->getMethod($methodName);

    	if(count($this->actuals) === count($args))
    	{
    		return $this->assertDifferentExpectations($method, $args);
    	}
    	$this->assertSameExpectations($method, $args);
    }

    /**
     * Test values against same expectations.
     *
     * @author	Andrea Marco Sartori
     * @param	string	$assertion
     * @param	array	$expectations
     * @return	void
     */
    protected function assertSameExpectations($assertion, $expectations)
    {
    	foreach ($this->actuals as $actual)
    	{
    		foreach ($expectations as $expectation)
    		{
    			call_user_func(array(__CLASS__, $assertion), $expectation, $actual);
    		}
    		if( ! $expectations) call_user_func(array(__CLASS__, $assertion), $actual);
    	}
    }

    /**
     * Test values against different expectations.
     *
     * @author	Andrea Marco Sartori
     * @param	string	$assertion
     * @param	array	$expectations
     * @return	void
     */
    protected function assertDifferentExpectations($assertion, $expectations)
    {
    	for ($i = 0; $i < count($expectations); $i++)
    	{
    		foreach ((array)$expectations[$i] as $expectation)
    		{
    			call_user_func(array(__CLASS__, $assertion), $expectation, $this->actuals[$i]);
    		}
    	}
    }

}