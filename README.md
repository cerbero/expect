# Expect
Wrapper extending PHPUnit assertions inspired by JeffreyWay / PHPUnit-Wrappers.

## Features
* Readable syntax
* Aliased methods
* User defined aliases
* Handle multiple assertions at once
* Handle multiple expectations at once
* Handle different expectations for each value to test

### Readable syntax
Expect was written with the purpose of making PHPUnit syntax as easy as writing phrases:

    :::php
    Expect::that(true)->isTrue();

---

### Aliased methods
Each PHPUnit method is wrapped by an easy to remember alias.

###### assertTrue

    :::php
    Expect::that(true)->isTrue();

###### assertFalse

    :::php
    Expect::that(false)->isFalse();

###### assertContains

    :::php
    $array = array('foo');

    Expect::that($array)->has('foo');

---

### User defined aliases
Users may add their own aliases to customize Expect by registering an alias-assertion pair array:

    :::php
    Expect::register(array('shouldBeTrue' => 'assertTrue'));

    Expect::that(true)->shouldBeTrue();

---

### Handling multiple assertions at once
Expect is able to run several assertions at once by adding multiple parameters in `that()`.

> You may want to write the plural form of aliases when performing multiple assertions: use `isTrue` / `areTrue`, `isFalse` / `areFalse` and so on interchangeably.

In the example below, two `assertTrue` will be triggered and the second one will fail asserting that false is true:

    :::php
    Expect::that(true, false)->areTrue();

In the example below, two `assertFalse` will be triggered and the second one will fail asserting that true is false:

    :::php
    Expect::that(false, true)->areFalse();

---

### Handling multiple expectations at once
Expect can manage several expectations at once by adding multiple parameters in aliased methods.

The example below will run two `assertContains` verifying both `foo` and `bar` are contained in `$array`:

    :::php
    $array = array('foo', 'bar');

    Expect::that($array)->has('foo', 'bar');

---

### Handling different expectations for each value to test
You may test more values against the same assertion with dedicated expectations per value.

This behaviour is adopted when the number of values to test equals the number of expectations.

The example below will run two `assertContains` verifying `foo` is contained in `$array1` and `bar` is contained in `$array2`:

    :::php
    $array1 = array('foo');
    $array2 = array('bar');

    Expect::that($array1, $array2)->have('foo', 'bar');

To test values against more dedicated expectations, simply use arrays.

The example below will run three `assertContains` verifying both `foo` and `bar` is contained in `$array1` and `baz` is contained in `$array2`:

    :::php
    $array1 = array('foo', 'bar');
    $array2 = array('baz');

    Expect::that($array1, $array2)->have(array('foo', 'bar'), 'baz');

---

## Tasks
* alias for assertArrayHasKey()
* alias for assertClassHasAttribute()
* alias for assertClassHasStaticAttribute()
* alias for assertContainsOnly()
* alias for assertContainsOnlyInstancesOf()
* alias for assertCount()
* alias for assertEmpty()
* alias for assertEqualXMLStructure()
* alias for assertEquals()
* alias for assertFileEquals()
* alias for assertFileExists()
* alias for assertGreaterThan()
* alias for assertGreaterThanOrEqual()
* alias for assertInstanceOf()
* alias for assertInternalType()
* alias for assertJsonFileEqualsJsonFile()
* alias for assertJsonStringEqualsJsonFile()
* alias for assertJsonStringEqualsJsonString()
* alias for assertLessThan()
* alias for assertLessThanOrEqual()
* alias for assertNull()
* alias for assertObjectHasAttribute()
* alias for assertRegExp()
* alias for assertStringMatchesFormat()
* alias for assertStringMatchesFormatFile()
* alias for assertSame()
* alias for assertSelectCount()
* alias for assertSelectEquals()
* alias for assertSelectRegExp()
* alias for assertStringEndsWith()
* alias for assertStringEqualsFile()
* alias for assertStringStartsWith()
* alias for assertTag()
* alias for assertThat()
* alias for assertXmlFileEqualsXmlFile()
* alias for assertXmlStringEqualsXmlFile()
* alias for assertXmlStringEqualsXmlString()